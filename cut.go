package cut

import (
	"encoding/json"
	"os"
	"os/exec"
	"strings"

	"github.com/kpango/glg"
)

type InputData struct {
	FileName  string `json:"filename_input"`
	Output    string `json:"filename_output"`
	StartTime string `json:"start_time"`
	StopTime  string `json:"stop_time"`
}

func Cut(respInput interface{}, srcPath, dstPath string) error {

	srcPath = strings.TrimSuffix(srcPath, "/")
	dstPath = strings.TrimSuffix(dstPath, "/")

	var inputData InputData
	infoByte, err := json.Marshal(respInput)
	if err != nil {
		glg.Fatalln(err)
	}

	err = json.Unmarshal(infoByte, &inputData)
	if err != nil {
		glg.Fatalln(err)
	}

	command := []string{"-y", "-i", srcPath + "/" + inputData.FileName, "-ss", inputData.StartTime, "-t", inputData.StopTime, "-c", "copy", dstPath + "/" + inputData.Output}

	glg.Debug(strings.Join(command[:], " "))

	cmd := exec.Command("ffmpeg", command...)
	//cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err = cmd.Run()
	if err != nil {
		return err
	}

	glg.Info("cut content", inputData.FileName, "from", inputData.StartTime, "to", inputData.StopTime, "success and output to", inputData.Output)

	return nil
}
